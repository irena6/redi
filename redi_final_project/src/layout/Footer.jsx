import React from "react";
import Typography from "@material-ui/core/Typography";

class Footer extends React.Component {
  render() {
    return (
      <>
        <Typography variant="subtitle1" align="center" component="p">
          This is our footer!
        </Typography>
      </>
    );
  }
}

export default Footer;
