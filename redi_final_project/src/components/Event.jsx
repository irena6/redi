import React from "react";

class Event extends React.Component {
  render() {
    /*
    console.log(this.props.allInfosOfEvent);
    console.log(this.props.name);
    */
    return (
      <>
        <b>This is a Single Event View for Eventlist Page</b>
        <br />
        {this.props.allInfosOfEvent.name}
        <br />
        {this.props.allInfosOfEvent.date}
        <br />
      </>
    );
  }
}

export default Event;
