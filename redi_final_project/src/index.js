import React from "react";
import ReactDOM from "react-dom";
import "./styles.css";

import EventList from "./pages/EventsList";

function App() {
  return (
    <div className="App">
      <EventList />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
